-module(p13).
-export([decode/1]).

decode(List) ->
	p05:reverse(decode(List, [])).

decode([], Acc) ->
	Acc;

decode([{1, Symbol} | T], Acc) ->
	decode(T, [Symbol | Acc]);

decode([{Num, Symbol} | T], Acc) ->
	decode([{Num - 1, Symbol} | T], [Symbol | Acc]).