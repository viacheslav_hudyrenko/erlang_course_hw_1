-module(p06).
-export([is_palindrom/1]).

is_palindrom(List) ->
	List =:= p05:reverse(List).
