-module(p03).
-export([element_at/2]).

element_at([H | _T], 1) -> H;

element_at([_H | T], Number) ->
    element_at(T, Number - 1);

element_at([], _) -> undef.
