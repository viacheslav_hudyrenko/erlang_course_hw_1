-module(p02).
-export([but_last/1]).

but_last([_, _ | []] = Last2) ->
    Last2;

but_last([_H | T]) -> 
    but_last(T).
