-module(p09).
-export([pack/1]).

pack(E = []) ->
	E;

pack([H | T]) ->
	p05:reverse(pack(T, _Local = [H], _Global = [])).

pack([H | T], Local = [H | _], Global) ->
	pack(T, [H | Local], Global);

pack([H | T], Local = [_ | _], Global) ->
	pack(T, [H], [Local | Global]);

pack([], Local, Global) ->
	[Local | Global].

	