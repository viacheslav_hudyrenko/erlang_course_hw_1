-module(p10).
-export([encode/1]).

encode(E = []) ->
	E;

encode([H | T]) ->
	p05:reverse(encode(T, {1, H}, [])).

encode([H | T], {Num, H}, Code) ->
	encode(T, {Num + 1, H}, Code);

encode([H | T], Elem, Code) ->
	encode(T, {1, H}, [Elem | Code]);

encode([], Elem, Code) ->
	[Elem | Code].

