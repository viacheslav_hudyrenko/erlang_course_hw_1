-module(p11).
-export([encode_modified/1]).

encode_modified(List) ->
	p05:reverse(clean(p10:encode(List), [])).

clean([], Acc) ->
	Acc;

clean([{1, Symbol} | T], Acc) ->
	clean(T, [Symbol | Acc]);

clean([H | T], Acc) ->
	clean(T, [H | Acc]).