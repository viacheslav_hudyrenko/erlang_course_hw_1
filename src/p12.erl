-module(p12).
-export([decode_modified/1]).

decode_modified(List) ->
	p05:reverse(decode_modified(List, [])).

decode_modified([], Acc) ->
	Acc;

decode_modified([{1, Symbol} | T], Acc) ->
	decode_modified(T, [Symbol | Acc]);

decode_modified([{Num, Symbol} | T], Acc) ->
	decode_modified([{Num - 1, Symbol} | T], [Symbol | Acc]);

decode_modified([Symbol | T], Acc) ->
	decode_modified(T, [Symbol | Acc]).