-module(p08).
-export([compress/1]).

compress(E = []) ->
	E;

compress([H | T]) ->
	p05:reverse(compress(T, [H])).

compress([], Acc) ->
	Acc;

compress([H | T], Acc = [H | _]) ->
	compress(T, Acc);

compress([H | T], Acc = [_ | _]) ->
	compress(T, [H | Acc]).