-module p05.
-export([reverse/1]).

reverse(List) ->
	reverse(List, []).

reverse([H | T], Res) ->
	reverse(T, [H | Res]);

reverse([], Res) ->
	Res.