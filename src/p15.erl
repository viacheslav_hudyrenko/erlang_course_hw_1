-module(p15).
-export([replicate/2]).

replicate(_List, Num) when Num < 0 ->
	undef;

replicate(_List, 0) ->
	[];

replicate(List, 1) ->
	List;

replicate(List, Num) ->
	p07:flatten(p05:reverse(replicate(List, Num, []))).

replicate([H | T], Num, Acc) ->
	replicate(T, Num, [replicateSymbol(H, Num, []) | Acc]);

replicate([], _Num, Acc) ->
	Acc.

replicateSymbol(_Symbol, 0, Acc) ->
	Acc;

replicateSymbol(Symbol, Num, Acc) ->
	replicateSymbol(Symbol, Num - 1, [Symbol | Acc]).